/* eslint-disable */

const { series, crossEnv, copy, concurrent, rimraf } = require("nps-utils");

module.exports = {
  scripts: {
    default: "nps webpack",
    test: {
      default: series("nps webpack.devEnv", "nps test.jest"),
      jest: {
        default: series(rimraf("test/coverage-jest"), "jest"),
        accept: "jest -u",
        watch: "jest --watch",
      },

      lint: {
        default: series("tsc --noEmit", 'eslint "src/**/*.ts" "test/**/*.ts"'),
        fix: "eslint src --fix",
      },
      all: concurrent({
        jest: "nps test.jest",
        lint: "nps test.lint",
      }),
    },
    build: "nps webpack.build",
    translations: 'i18next-scanner "src/**/*.{js,ts,html}"',
    webpack: {
      default: "nps webpack.server",
      devEnv: copy("--rename environment.ts ./aurelia_project/environments/dev.ts ./src/"),
      prodEnv: copy("--rename environment.ts ./aurelia_project/environments/prod.ts ./src/"),
      build: {
        before: rimraf("dist"),
        default: "nps webpack.build.production",
        development: {
          default: series(
            "nps webpack.build.before",
            "nps webpack.devEnv",
            "webpack --progress -d",
          ),
          extractCss: series("nps webpack.build.before", "webpack --progress -d --env.extractCss"),
          serve: series.nps("webpack.build.development", "serve"),
        },
        production: {
          inlineCss: series(
            "nps webpack.build.before",
            crossEnv("NODE_ENV=production webpack --progress -p --env.production"),
          ),
          default: series(
            "nps webpack.build.before",
            "nps webpack.prodEnv",
            crossEnv("NODE_ENV=production webpack --progress -p --env.production --env.extractCss"),
            "git checkout .",
          ),
          serve: series.nps("webpack.build.production", "serve"),
        },
      },
      server: {
        default: series(
          "nps webpack.devEnv",
          `webpack-dev-server -d --devtool '#source-map' --inline --env.server`,
        ),
        extractCss: `webpack-dev-server -d --devtool '#source-map' --inline --env.server --env.extractCss`,
        hmr: `webpack-dev-server -d --devtool '#source-map' --inline --hot --env.server`,
      },
    },
    serve: "http-server dist --cors",
  },
};
