/* tslint:disable:max-classes-per-file */
/* eslint-disable @typescript-eslint/no-unused-vars */

export class I18NStub {
    public tr(key: string, _options?: never): string {
        return key;
    }
}

type NavigationConfig = {
    name: string;
    settings?: {
        auth: boolean;
    };
};

type NavigationConfigInstruction = {
    config: NavigationConfig;
};

export class NavigationInstruction {
    public constructor(
        public instructions: NavigationConfigInstruction[],
        public config: NavigationConfig,
    ) {}

    public getAllInstructions(): NavigationConfigInstruction[] {
        return this.instructions;
    }
}
