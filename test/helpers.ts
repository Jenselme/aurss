import { Aurelia } from "aurelia-framework";
import { ComponentTester } from "aurelia-testing";

class DfValueConverter {
    toView(value: Date) {
        return `${value.getFullYear()}-${value.getUTCMonth()}-${value.getUTCDay()}`;
    }
}

export const prepareI18nComponent = (component: ComponentTester): void => {
    component.bootstrap((aurelia: Aurelia) => {
        return aurelia.use.standardConfiguration().globalResources(DfValueConverter);
    });
};

export const selectElementWithText = (
    document: Document,
    selector: string,
    text: string,
): HTMLElement => {
    const nodeList: HTMLElement[] = Array.from(document.querySelectorAll(selector));
    return nodeList.filter((element) => element.textContent?.trim() === text)[0];
};
