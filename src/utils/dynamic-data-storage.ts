import { IDBPDatabase, openDB } from "idb";
import * as constants from "../constants";

const DEFAULT_STORE_NAME = "aurss-default-store";

type Storage = {
    write: (data: Record<string, string>) => Promise<void>;
};

export async function makeStorage(storeName: string = DEFAULT_STORE_NAME): Promise<Storage> {
    const db = await openDB(constants.DYNAMIC_DB_NAME, constants.DYNAMIC_DB_SCHEMA_VERSION, {
        upgrade: (database: IDBPDatabase) => {
            database.createObjectStore(storeName, { autoIncrement: true, keyPath: "id" });
        },
    });

    return Object.freeze({
        async write(data) {
            await db.add(storeName, data);
        },
    });
}
