export interface Article {
    readonly author: string;
    readonly feedId: number;
    readonly feedTitle: string;
    readonly id: number;
    isRead: boolean;
    isFavorite: boolean;
    readonly link: string;
    readonly title: string;
    readonly description: string;
    readonly updatedAt: Date;
}
