import { rehydrateFromLocalStorage } from "aurelia-store";
import { State } from "./state";

export function rehydratePartiallyFromLocalStorage(state: State, key?: string): State {
    const stateFromLocalStorage: State = rehydrateFromLocalStorage(state, key);
    const newState = { ...state };
    if (stateFromLocalStorage.authentication) {
        newState.authentication = stateFromLocalStorage.authentication;
    }
    if (stateFromLocalStorage.options) {
        newState.options = stateFromLocalStorage.options;
    }

    return newState;
}
