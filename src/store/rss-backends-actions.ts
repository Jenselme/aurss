import { produce } from "immer";
import { Container } from "aurelia-framework";
import * as LogManager from "aurelia-logging";
import { Store } from "aurelia-store";
import * as constants from "../constants";
import { Article } from "../models/Article";
import { Category } from "../models/Category";
import { Counter } from "../models/Counter";
import { Backend } from "../services/backend";
import { TestBackend } from "../services/backends/test";
import { TTRss } from "../services/backends/ttrss";
import { doRouterNavigation } from "./actions";
import { Auth, SelectableBackend, State } from "./state";

const logger = LogManager.getLogger("aurss:store:actions:rss");

export const selectBackend = produce((state: State, selectedBackend: SelectableBackend) => {
    state.selectedBackend = selectedBackend;
});

/**
 * This action is meant to be called when the authenticate method of the backend succeeds.
 * @param state
 * @param authInfos
 */

export function authenticateSucceeded(state: State, authInfos: Auth): State {
    const newState = Object.assign({}, state);
    newState.authentication = authInfos;
    return doRouterNavigation(newState, "display-articles-from-category", {
        categoryId: constants.DEFAULT_CATEGORY,
    });
}

/**
 * This action is meant to be called when the isLoggedIn method of the backend returns true.
 * @param state
 */
export const isLoggedIn = produce((state: State) => {
    state.authentication.isLoggedIn = true;
});

export const fetchArticles = produce((state: State, categoryId: string) => {
    state.rss.isFetching = true;

    getBackend(state.selectedBackend)
        .getArticles(categoryId)
        .catch((error) => logger.error(error));
});

export const receivedArticles = produce((state: State, articles: Article[]) => {
    state.rss.isFetching = false;
    state.rss.displayedArticles = articles;
});

export function fetchCategories(state: State): State {
    getBackend(state.selectedBackend).getCategories();
    return state;
}

export const receivedCategories = produce((state: State, categories: Category[]) => {
    state.rss.categories = categories;
});

export function fetchCounters(state: State): State {
    getBackend(state.selectedBackend)
        .getCounters()
        .catch((error) => logger.error(error));
    return state;
}

export const receivedCounters = produce((state: State, counters: Counter[]) => {
    const categoryIdToCounters = counters.reduce((acc, counter) => {
        acc[counter.categoryId] = counter.unreadCount;
        return acc;
    }, {});

    state.rss.categories = state.rss.categories.map((category) => ({
        ...category,
        unreadCount: categoryIdToCounters[category.id],
    }));
});

/**
 * Do a request to the backend to mark an article as read.
 * @param state
 * @param article
 * @param discard: If this is true, the article will be added to articlesToDiscards.
 */
export const markAsRead = produce((state: State, article: Article, { discard = false } = {}) => {
    getBackend(state.selectedBackend)
        .markAsRead(article)
        .catch((error) => logger.error(error));

    if (!discard) {
        return;
    }

    state.rss.articlesToDiscards.push(article);
});

export function markAsUnread(state: State, article: Article): State {
    getBackend(state.selectedBackend)
        .markAsUnread(article)
        .catch((error) => logger.error(error));
    return state;
}

export function markAsFavorite(state: State, article: Article): State {
    getBackend(state.selectedBackend)
        .markAsFavorite(article)
        .catch((error) => logger.error(error));
    return state;
}

export function unmarkAsFavorite(state: State, article: Article): State {
    getBackend(state.selectedBackend)
        .unmarkAsFavorite(article)
        .catch((error) => logger.error(error));
    return state;
}

export const markedAsRead = produce((state: State, article: Article) => {
    fetchCounters(state);

    if (state.rss.articlesToDiscards.includes(article)) {
        removeArticle(state.rss.displayedArticles, article);
        return;
    }

    const articleToUpdate = findArticleInArray(state.rss.displayedArticles, article);
    if (articleToUpdate) {
        articleToUpdate.isRead = true;
    }
});

function removeArticle(articles: Article[], article: Article) {
    const articleToRemove = findArticleInArray(articles, article);
    if (articleToRemove) {
        const indexToRemove = articles.indexOf(articleToRemove);
        articles.splice(indexToRemove, 1);
    }
}

function findArticleInArray(articles: Article[], article: Article): Article | undefined {
    return articles.find((elt) => elt.id === article.id);
}

export const markedAsUnread = produce((state: State, article: Article) => {
    fetchCounters(state);

    const articleToUpdate = findArticleInArray(state.rss.displayedArticles, article);
    if (articleToUpdate) {
        articleToUpdate.isRead = false;
    }
});

export const markedAsFavorite = produce((state: State, article: Article) => {
    const articleToUpdate = findArticleInArray(state.rss.displayedArticles, article);
    if (articleToUpdate) {
        articleToUpdate.isFavorite = true;
    }
});

export const unmarkedAsFavorite = produce((state: State, article: Article) => {
    const articleToUpdate = findArticleInArray(state.rss.displayedArticles, article);
    if (articleToUpdate) {
        articleToUpdate.isFavorite = false;
    }
});

export function openArticle(state: State, article: Article): State {
    const tab = window.open(article.link, "_blank");
    if (tab) {
        tab.focus();
    }

    return state;
}

export function getBackend(selectedBackend: SelectableBackend): Backend {
    switch (selectedBackend) {
        case SelectableBackend.ttrss:
            return Container.instance.get(TTRss) as Backend;
        case SelectableBackend.test:
            return Container.instance.get(TestBackend) as Backend;
        default:
            logger.error("An unsupported backend was requested, return the default test backend");
            return Container.instance.get(TestBackend) as Backend;
    }
}

// This must be exported because to tests some component that uses dispatchify,
// these actions must be registered.
export function registerRssBackendsActions(store: Store<State>): void {
    store.registerAction("selectBackend", selectBackend);
    store.registerAction("authenticateSucceeded", authenticateSucceeded);
    store.registerAction("isLoggedIn", isLoggedIn);
    store.registerAction("markAsRead", markAsRead);
    store.registerAction("markAsUnread", markAsUnread);
    store.registerAction("markedAsRead", markedAsRead);
    store.registerAction("markAsFavorite", markAsFavorite);
    store.registerAction("unmarkAsFavorite", unmarkAsFavorite);
    store.registerAction("markedAsUnread", markedAsUnread);
    store.registerAction("markedAsFavorite", markedAsFavorite);
    store.registerAction("unmarkedAsFavorite", unmarkedAsFavorite);
    store.registerAction("openArticle", openArticle);
    store.registerAction("fetchArticles", fetchArticles);
    store.registerAction("receivedArticles", receivedArticles);
    store.registerAction("fetchCategories", fetchCategories);
    store.registerAction("receivedCategories", receivedCategories);
    store.registerAction("fetchCounters", fetchCounters);
    store.registerAction("receivedCounters", receivedCounters);
}
