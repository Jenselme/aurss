import { bindable } from "aurelia-framework";
import { Options } from "../../store/state";

export class AurssOptions {
    @bindable() public onToggleOption: (optionName: keyof Options) => void;
    @bindable() public options: Options;

    public toggleMarkAsReadOnScroll(): void {
        this.onToggleOption("markAsReadOnScroll");
    }

    public toggleDisplayDescriptions(): void {
        this.onToggleOption("displayDescriptions");
    }

    public toggleViewOnlyUnread(): void {
        this.onToggleOption("viewOnlyUnread");
    }
}
