import { bindable } from "aurelia-framework";

export class AurssCategoryLinks {
    @bindable() public categories;
    @bindable() public currentCategoryId;
    // eslint-disable-next-line max-len
    // eslint-disable-next-line @typescript-eslint/no-unused-vars,@typescript-eslint/no-empty-function
    @bindable() public onActiveClicked = (_categoryId: string): void => {};

    public dispatchOnActiveClicked(event: Event, clickedCategoryId: string): boolean {
        if (this.currentCategoryId !== clickedCategoryId) {
            // Do default action for event, don't prevent it.
            return true;
        }

        event.stopPropagation();
        this.onActiveClicked(clickedCategoryId);
        return false;
    }
}
