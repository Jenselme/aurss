export const DEFAULT_CATEGORY = "unread";
export const DYNAMIC_DB_NAME = "aurss";
export const DYNAMIC_DB_SCHEMA_VERSION = 1;
export const DEFAULT_CATEGORIES_TO_REAL_CATEGORY_ID = {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    TTRSS: {
        [DEFAULT_CATEGORY]: "-3",
    },
};
// In ms.
export const REFRESH_INTERVAL = 30 * 60 * 1000;
export const STATE_STORAGE_KEY = "aurss-storage";
export const OPTIONS_STORAGE_KEY = "aurss-options";
export const TTRSS_ACTIONS_STORAGE_NAME = "ttrss-actions";
// This is used by the ServiceWorker.
export const VERSION = "1.2.1";
